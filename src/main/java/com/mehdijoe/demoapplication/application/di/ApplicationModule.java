package com.mehdijoe.demoapplication.application.di;

import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;

import com.mehdijoe.demoapplication.application.DemoApplication;
import com.mehdijoe.demoapplication.model.api.AlbumRemoteRepository;
import com.mehdijoe.demoapplication.model.api.AlbumRemoteRepositoryHandler;
import com.mehdijoe.demoapplication.model.api.PhotoRemoteRepository;
import com.mehdijoe.demoapplication.model.api.PhotoRemoteRepositoryHandler;
import com.mehdijoe.demoapplication.model.database.CacheDatabase;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.model.repository.AlbumRepository;
import com.mehdijoe.demoapplication.model.repository.AlbumRepositoryHandler;
import com.mehdijoe.demoapplication.model.repository.PhotoRepository;
import com.mehdijoe.demoapplication.model.repository.PhotoRepositoryHandler;
import com.mehdijoe.demoapplication.util.Constant;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joe on 24/10/2018.
 */

@Module
public class ApplicationModule {
    private DemoApplication demoApplication;

    public ApplicationModule(@NonNull DemoApplication demoApplication){
        this.demoApplication = demoApplication;
    }

    @Singleton
    @Provides
    public AlbumRemoteRepositoryHandler provideAlbumWebservice(Retrofit retrofit) {
        return new AlbumRemoteRepository(retrofit);
    }

    @Singleton
    @Provides
    public PhotoRemoteRepositoryHandler providePhotoWebservice(Retrofit retrofit) {
        return new PhotoRemoteRepository(retrofit);
    }

    @Singleton
    @Provides
    public CacheDatabase provideCacheDatabase() {
        return Room.databaseBuilder(demoApplication.getApplicationContext(),
                CacheDatabase.class, "cache_db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Singleton
    @Provides
    public AlbumRepositoryHandler provideAlbumRepository(CacheDatabase cacheDatabase, AlbumRemoteRepositoryHandler albumRemoteRepository){
        return new AlbumRepository(cacheDatabase, albumRemoteRepository);
    }

    @Singleton
    @Provides
    public PhotoRepositoryHandler providePhotoRepository(CacheDatabase cacheDatabase, PhotoRemoteRepositoryHandler photoRemoteRepository){
        return new PhotoRepository(cacheDatabase, photoRemoteRepository);
    }

    @Singleton
    @Provides
    public Retrofit provideRetrofit(GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    public GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    public MutableLiveData<Boolean> provideBooleanMutableLiveData() {
        return new MutableLiveData<Boolean>();
    }

    @Provides
    public MutableLiveData<List<Album>> provideAlbumsMutableLiveData() {
        return new MutableLiveData<List<Album>>();
    }

    @Provides
    public MutableLiveData<List<Photo>>  providePhotosMutableLiveData() {
        return new MutableLiveData<List<Photo>>();
    }
}
