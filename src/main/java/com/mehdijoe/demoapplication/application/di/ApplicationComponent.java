package com.mehdijoe.demoapplication.application.di;


import com.mehdijoe.demoapplication.viewmodel.AlbumViewModel;
import com.mehdijoe.demoapplication.viewmodel.PhotoViewModel;


import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Joe on 24/10/2018.
 */

@Singleton
@Component (modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(AlbumViewModel albumViewModel);
    void inject(PhotoViewModel photoViewModel);
}
