package com.mehdijoe.demoapplication.application;

import android.app.Application;

import com.mehdijoe.demoapplication.application.di.ApplicationComponent;
import com.mehdijoe.demoapplication.application.di.DaggerApplicationComponent;
import com.mehdijoe.demoapplication.application.di.ApplicationModule;

/**
 * Created by Joe on 24/10/2018.
 */

public class DemoApplication extends Application {

    private ApplicationComponent component;

    public ApplicationComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildComponent();
    }

    public void buildComponent() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
