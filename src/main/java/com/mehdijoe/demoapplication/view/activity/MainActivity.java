package com.mehdijoe.demoapplication.view.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mehdijoe.demoapplication.R;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.view.adapter.AlbumAdapter;
import com.mehdijoe.demoapplication.view.adapter.PhotoAdapter;
import com.mehdijoe.demoapplication.viewmodel.AlbumViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements AlbumAdapter.OnAlbumAdapterClickListener {
    private static final String TAG = "MainActivity";

    private AlbumViewModel albumViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        //init viewmodel
        albumViewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);

        //init recycler
        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(this, getResources().getInteger(R.integer.grid_columns)));
        recyclerView.setHasFixedSize(true);

        //init adapter
        AlbumAdapter albumAdapter = new AlbumAdapter(this);
        recyclerView.setAdapter(albumAdapter);

        //update data
        onDataUpdate(albumAdapter);
    }

    private void onDataUpdate(final AlbumAdapter albumAdapter){
        albumViewModel.getAlbums().observe(this, new Observer<List<Album>>() {
            @Override
            public void onChanged(@Nullable List<Album> albums) {
                Log.d(TAG, "albums > onChange : " + albums);
                albumAdapter.setAlbums(albums);
            }
        });

        albumViewModel.getFetchingAlbums().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean inWaitingProgress) {
                Log.d(TAG, "inWaitingProgress > onChange : " + inWaitingProgress);
                MainActivity.this.findViewById(R.id.progress)
                        .setVisibility(inWaitingProgress ? View.VISIBLE : View.GONE);
            }
        });

        albumViewModel.getInvalidDataInLastConnection().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean invalidDatainLastConnection) {
                Log.d(TAG, "invalidDatainLastConnection > onChange : " + invalidDatainLastConnection);
                MainActivity.this.findViewById(R.id.retry_layout)
                        .setVisibility(invalidDatainLastConnection ? View.VISIBLE : View.GONE);
            }
        });

        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.albumViewModel.loadAlbums();
            }
        });

        ((SwipeRefreshLayout)findViewById(R.id.pull_to_refresh)).setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(MainActivity.this, R.string.data_refreshed, Toast.LENGTH_LONG).show();
                albumViewModel.refreshPhotos();
                ((SwipeRefreshLayout)MainActivity.this.findViewById(R.id.pull_to_refresh)).setRefreshing(false);
            }
        });
    }

    @Override
    public void onAlbumItemClicked(Album album) {
        Intent intent = new Intent(MainActivity.this, AlbumActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(AlbumActivity.PARAM_ALBUM_ID, album.getId());
        bundle.putString(AlbumActivity.PARAM_ALBUM_NAME, album.getTitle());
        intent.putExtras(bundle);
        startActivity(intent);
    }
}