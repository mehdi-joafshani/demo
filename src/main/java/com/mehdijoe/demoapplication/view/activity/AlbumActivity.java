package com.mehdijoe.demoapplication.view.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mehdijoe.demoapplication.R;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.view.adapter.PhotoAdapter;
import com.mehdijoe.demoapplication.viewmodel.PhotoViewModel;

import java.util.List;


public class AlbumActivity extends AppCompatActivity {

    private static final String TAG = "AlbumActivity";
    public static final String PARAM_ALBUM_ID = "album_id";
    public static final String PARAM_ALBUM_NAME = "album_name";

    private PhotoViewModel photoViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        init();
    }

    private void init() {
        //init viewmodel
        photoViewModel = ViewModelProviders.of(this).get(PhotoViewModel.class);

        //get parent album data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            photoViewModel.setAlbumId(bundle.getInt(PARAM_ALBUM_ID));
            setTitle(bundle.getString(PARAM_ALBUM_NAME));
        }

        //init recycler
        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(this, getResources().getInteger(R.integer.grid_columns)));
        recyclerView.setHasFixedSize(true);

        //init adapter
        PhotoAdapter photoAdapter = new PhotoAdapter();
        recyclerView.setAdapter(photoAdapter);

        //init data which are updated by viewmodel
        initDataUpdate(photoAdapter);
    }

    private void initDataUpdate(final PhotoAdapter photoAdapter){
        photoViewModel.getPhotos().observe(this, new Observer<List<Photo>>() {
            @Override
            public void onChanged(@Nullable List<Photo> photos) {
                Log.d(TAG, "photos > onChange : " + photos);
                photoAdapter.setPhotos(photos);
            }
        });

        photoViewModel.getFetchingPhotos().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean inWaitingProgress) {
                Log.d(TAG, "inWaitingProgress > onChange : " + inWaitingProgress);
                AlbumActivity.this.findViewById(R.id.progress)
                        .setVisibility(inWaitingProgress ? View.VISIBLE : View.GONE);
            }
        });

        photoViewModel.getInvalidDataInLastConnection().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean invalidDatainLastConnection) {
                Log.d(TAG, "invalidDatainLastConnection > onChange : " + invalidDatainLastConnection);
                AlbumActivity.this.findViewById(R.id.retry_layout)
                        .setVisibility(invalidDatainLastConnection ? View.VISIBLE : View.GONE);
            }
        });

        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumActivity.this.photoViewModel.loadPhotos();
            }
        });

        ((SwipeRefreshLayout) findViewById(R.id.pull_to_refresh)).setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(AlbumActivity.this, R.string.data_refreshed, Toast.LENGTH_LONG).show();
                photoViewModel.refreshPhotos();
                ((SwipeRefreshLayout)AlbumActivity.this.findViewById(R.id.pull_to_refresh)).setRefreshing(false);
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
