package com.mehdijoe.demoapplication.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mehdijoe.demoapplication.R;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.util.Constant;
import com.mehdijoe.demoapplication.viewmodel.PhotoViewModel;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumsCoverPhotoLoadEvent;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Joe on 24/10/2018.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumHolder> {

    /**
     * AlbumAdapter logging tag
     */
    private static final String TAG = "AlbumAdapter";

    /**
     * the list of albums which is going to be displayed
     */
    private List<Album> albums = new ArrayList<>();

    /**
     * The cover photos of adapter albums
     */
    private Map<Integer, Photo> coverPhotos = new HashMap<>();

    /**
     * OnAlbumAdapterClickListener to handle clicks on item, outside of adapter
     */
    private OnAlbumAdapterClickListener onAlbumAdapterClickListener;

    public AlbumAdapter(OnAlbumAdapterClickListener onAlbumAdapterClickListener) {
        this.onAlbumAdapterClickListener = onAlbumAdapterClickListener;
        EventBus.getDefault().register(this);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @NonNull
    @Override
    public AlbumHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_album, viewGroup, false);
        return new AlbumHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumHolder albumHolder, int position) {
        final Album album = albums.get(position);
        albumHolder.albumTitleTextView.setText(album.getTitle());

        albumHolder.albumCoverImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAlbumAdapterClickListener.onAlbumItemClicked(album);
            }
        });

        if (coverPhotos.get(album.getId()) != null) {
            Picasso.get()
                    .load(coverPhotos.get(album.getId()).getThumbnailUrl())
                    .error(R.drawable.ic_image_black_100dp)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(albumHolder.albumCoverImageView);
        }else{
            albumHolder.albumCoverImageView.setImageResource(R.drawable.ic_image_black_100dp);
        }
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;

        //TODO: change this approach
        notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlbumsCoverPhotoLoaded(AlbumsCoverPhotoLoadEvent event) {
        for (Photo coverPhoto : event.getCoverPhotos()) {
            coverPhotos.put(coverPhoto.getAlbumId(), coverPhoto);
        }

        //TODO: Change this approach
        notifyDataSetChanged();

        EventBus.getDefault().unregister(this);

    }

    class AlbumHolder extends RecyclerView.ViewHolder {

        private ImageView albumCoverImageView;
        private TextView albumTitleTextView;

        public AlbumHolder(@NonNull View itemView) {
            super(itemView);

            albumCoverImageView = itemView.findViewById(R.id.album_cover_imageview);
            albumTitleTextView = itemView.findViewById(R.id.album_title_textview);
        }
    }

    public interface OnAlbumAdapterClickListener {
        void onAlbumItemClicked(Album album);
    }
}
