package com.mehdijoe.demoapplication.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mehdijoe.demoapplication.R;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.util.Constant;
import com.mehdijoe.demoapplication.viewmodel.PhotoViewModel;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder>{

    /**
     * AlbumAdapter logging tag
     */
    private static final String TAG = "PhotoAdapter";

    /**
     * the list of albums which is going to be displayed
     */
    private List<Photo> photos = new ArrayList<>();

    @NonNull
    @Override
    public PhotoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_photo, viewGroup, false);
        return new PhotoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoHolder albumHolder, int position) {
        Photo photo = photos.get(position);
        albumHolder.albumTitleTextView.setText(photo.getTitle());

        Picasso.get()
                .load(photo.getThumbnailUrl())
                .error(R.drawable.ic_image_black_100dp)
                .into(albumHolder.albumCoverImageView);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;

        //TODO: change this approach
        notifyDataSetChanged();
    }

    class PhotoHolder extends RecyclerView.ViewHolder{

        private ImageView albumCoverImageView;
        private TextView albumTitleTextView;

        public PhotoHolder(@NonNull View itemView) {
            super(itemView);

            albumCoverImageView = itemView.findViewById(R.id.photo_thumbnail_imageview);
            albumTitleTextView = itemView.findViewById(R.id.photo_title_textview);
        }
    }
}
