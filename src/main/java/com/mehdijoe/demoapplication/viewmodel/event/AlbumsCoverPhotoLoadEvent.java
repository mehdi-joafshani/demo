package com.mehdijoe.demoapplication.viewmodel.event;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Photo;

import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

public class AlbumsCoverPhotoLoadEvent {
    private List<Photo> coverPhotos;
    private DataType dataType;

    public AlbumsCoverPhotoLoadEvent(List<Photo> coverPhotos, DataType dataType) {
        this.coverPhotos = coverPhotos;
        this.dataType = dataType;
    }

    public DataType getDataType() {
        return dataType;
    }

    public List<Photo> getCoverPhotos() {
        return coverPhotos;
    }
}
