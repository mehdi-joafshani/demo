package com.mehdijoe.demoapplication.viewmodel.event;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Album;

import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

/**
 * This event class is used to broadcast album list change
 */
public class AlbumListLoadEvent {
    /**
     * The list of new albums
     */
    private List<Album> albums;

    /**
     * DataType enum to have more information about the albumList
     */
    private DataType dataType;

    public AlbumListLoadEvent(List<Album> albums) {
        this.albums = albums;
        this.dataType = DataType.notSpecified;
    }

    public AlbumListLoadEvent(List<Album> albums, DataType dataType) {
        this.dataType = dataType;
        this.albums = albums;
    }

    public AlbumListLoadEvent(boolean error) {
        this.albums = albums;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public DataType getDataType() {
        return dataType;
    }
}
