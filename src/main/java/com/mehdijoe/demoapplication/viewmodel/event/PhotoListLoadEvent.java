package com.mehdijoe.demoapplication.viewmodel.event;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.entity.Photo;

import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

/**
 * This event class is used to broadcast photo list change
 */
public class PhotoListLoadEvent {
    /**
     * The list of new photos
     */
    private List<Photo> photos;

    /**
     * DataType enum to have more information about the albumList
     */
    private DataType dataType;

    public PhotoListLoadEvent(List<Photo> photos) {
        this.photos = photos;
        this.dataType = DataType.notSpecified;
    }

    public PhotoListLoadEvent(List<Photo> photos, DataType dataType) {
        this.dataType = dataType;
        this.photos = photos;
    }

    public PhotoListLoadEvent(boolean error) {
        this.photos = photos;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public DataType getDataType() {
        return dataType;
    }
}
