package com.mehdijoe.demoapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mehdijoe.demoapplication.application.DemoApplication;
import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.repository.AlbumRepository;
import com.mehdijoe.demoapplication.model.repository.PhotoRepository;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumListLoadEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public class AlbumViewModel extends AndroidViewModel {
    private static final String TAG = "AlbumViewModel";

    @Inject AlbumRepository albumRepository;
    @Inject PhotoRepository photoRepository;
    @Inject MutableLiveData<List<Album>> albums;
    @Inject MutableLiveData<Boolean> fetchingAlbums;
    @Inject MutableLiveData<Boolean> invalidDataInLastConnection;

    @Inject
    public AlbumViewModel(@NonNull Application application) {
        super(application);

        Log.d(TAG, "initiation");

        ((DemoApplication)getApplication()).getComponent().inject(this);
        EventBus.getDefault().register(this);
        loadAlbums();
    }

    @Override
    protected void onCleared() {
        Log.d(TAG, "onCleared");
        EventBus.getDefault().unregister(this);
        super.onCleared();
    }

    public LiveData<List<Album>> getAlbums() {
        return albums;
    }

    public MutableLiveData<Boolean> getFetchingAlbums() {
        return fetchingAlbums;
    }

    public MutableLiveData<Boolean> getInvalidDataInLastConnection() {
        return invalidDataInLastConnection;
    }

    public void loadAlbums(){
        albumRepository.getAlbums();
        fetchingAlbums.setValue(true);
        invalidDataInLastConnection.setValue(false);
    }

    public void refreshPhotos(){
        albumRepository.refreshAlbums();
        fetchingAlbums.setValue(true);
        invalidDataInLastConnection.setValue(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlbumListUpdated(AlbumListLoadEvent event) {
        Log.d(TAG, "onAlbumListUpdated");
        this.albums.setValue(event.getAlbums());
        this.fetchingAlbums.setValue(false);
        if(event.getDataType() == DataType.invalidData){
            this.invalidDataInLastConnection.setValue(true);
        }else{
            photoRepository.getCoverPhotoPerAlbum();
        }
    }
}
