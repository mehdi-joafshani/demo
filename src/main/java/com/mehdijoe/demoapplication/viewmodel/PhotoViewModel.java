package com.mehdijoe.demoapplication.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mehdijoe.demoapplication.application.DemoApplication;
import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.model.repository.PhotoRepository;
import com.mehdijoe.demoapplication.viewmodel.event.PhotoListLoadEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public class PhotoViewModel extends AndroidViewModel {
    private static final String TAG = "PhotoViewModel";

    @Inject PhotoRepository photoRepository;
    @Inject MutableLiveData<List<Photo>> photos;
    @Inject MutableLiveData<Boolean> fetchingPhotos;
    @Inject MutableLiveData<Boolean> invalidDataInLastConnection;

    private int albumId;

    @Inject
    public PhotoViewModel(@NonNull Application application) {
        super(application);
        Log.d(TAG, "initiation");

        ((DemoApplication)getApplication()).getComponent().inject(this);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCleared() {
        Log.d(TAG, "onCleared");
        EventBus.getDefault().unregister(this);
        super.onCleared();
    }

    public MutableLiveData<Boolean> getFetchingPhotos() {
        return fetchingPhotos;
    }

    public LiveData<List<Photo>> getPhotos() {
        return photos;
    }

    public MutableLiveData<Boolean> getInvalidDataInLastConnection() {
        return invalidDataInLastConnection;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
        loadPhotos();
    }

    public void loadPhotos(){
        photoRepository.getPhotos(albumId);
        fetchingPhotos.setValue(true);
        invalidDataInLastConnection.setValue(false);
    }

    public void refreshPhotos(){
        photoRepository.refreshPhotos(albumId);
        fetchingPhotos.setValue(true);
        invalidDataInLastConnection.setValue(false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPhotoListUpdated(PhotoListLoadEvent event) {
        Log.d(TAG, "onPhotoListUpdated");
        this.photos.setValue(event.getPhotos());
        this.fetchingPhotos.setValue(false);
        if(event.getDataType() == DataType.invalidData){
            this.invalidDataInLastConnection.setValue(true);
        }
    }
}
