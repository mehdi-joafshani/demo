package com.mehdijoe.demoapplication.model.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Joe on 24/10/2018.
 */

@Entity(foreignKeys = @ForeignKey(entity = Album.class,
        parentColumns = "id",
        childColumns = "albumId",
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE)
        , indices = {@Index("albumId")})
public class Photo {
    /**
     * the id of the album which the photo belongs to
     */
    private int albumId;

    /**
     * our primary key which is not auto increment to sync with server data
     */
    @PrimaryKey
    private int id;

    /**
     * the title of the photo
     */
    private String title;

    /**
     * the url of the photo which gives the photo's file
     */
    private String url;

    /**
     * the url of the photo which gives a thumbnail copy of the photo
     */
    private String thumbnailUrl;

    public Photo(int albumId, int id, String title, String url, String thumbnailUrl) {
        this.albumId = albumId;
        this.id = id;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
