package com.mehdijoe.demoapplication.model.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Joe on 24/10/2018.
 */

@Entity
public class Album {

    /**
    * userId is the id of the user who created the album
     * TODO: enable foreign key for this attribute after bringing 'user's to the game
     */
    private int userId;

    /**
     * id (album id) is not auto increment as long as we are syncing out local data with server data
     */
    @PrimaryKey
    private int id;

    /**
     * tile of the album
     */
    private String title;

    public Album(int userId, int id, String title) {
        this.userId = userId;
        this.id = id;
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
