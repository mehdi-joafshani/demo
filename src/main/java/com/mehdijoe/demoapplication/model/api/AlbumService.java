package com.mehdijoe.demoapplication.model.api;

import com.mehdijoe.demoapplication.model.entity.Album;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Joe on 24/10/2018.
 */

public interface AlbumService {

    @GET("albums")
    Call<List<Album>> getAllAlbums();

}
