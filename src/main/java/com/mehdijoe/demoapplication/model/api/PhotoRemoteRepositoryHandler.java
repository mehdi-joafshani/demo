package com.mehdijoe.demoapplication.model.api;

import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.model.event.PhotoListRefreshEvent;
import com.mehdijoe.demoapplication.viewmodel.event.PhotoListLoadEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Joe on 24/10/2018.
 */

public interface PhotoRemoteRepositoryHandler {
    void getPhotos(int albumId);
}
