package com.mehdijoe.demoapplication.model.api;

import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.model.event.PhotoListRefreshEvent;
import com.mehdijoe.demoapplication.util.Constant;
import com.mehdijoe.demoapplication.viewmodel.event.PhotoListLoadEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joe on 24/10/2018.
 */

public class PhotoRemoteRepository implements PhotoRemoteRepositoryHandler {
    private static final String TAG = "PhotoRemoteRepository";

    private Retrofit retrofit;

    @Inject
    public PhotoRemoteRepository(Retrofit retrofit){
        this.retrofit = retrofit;
    }

    public void getPhotos(final int albumId){
        PhotoService photoService = retrofit.create(PhotoService.class);
        Call<List<Photo>> photoServiceCall = photoService.getAllPhotos(albumId);
        photoServiceCall.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                List<Photo> photos = response.body();

                EventBus.getDefault().post(new PhotoListLoadEvent(photos, DataType.onlineData));
                EventBus.getDefault().post(new PhotoListRefreshEvent(photos, albumId));
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Log.e(TAG, "fetch photo list from webservice error", t.getCause());

                EventBus.getDefault().post(new PhotoListLoadEvent(new ArrayList<Photo>(), DataType.invalidData));
            }
        });
    }
}
