package com.mehdijoe.demoapplication.model.api;

import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.event.AlbumListRefreshEvent;
import com.mehdijoe.demoapplication.util.Constant;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumListLoadEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Joe on 24/10/2018.
 */

public class AlbumRemoteRepository implements AlbumRemoteRepositoryHandler{
    private static final String TAG = "AlbumRemoteRepository";

    private Retrofit retrofit;

    @Inject
    public AlbumRemoteRepository(Retrofit retrofit){
        this.retrofit = retrofit;
    }

    public void getAlbums(){
        AlbumService albumService = retrofit.create(AlbumService.class);
        Call<List<Album>> albumServiceCall = albumService.getAllAlbums();
        albumServiceCall.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                List<Album> albums = response.body();

                EventBus.getDefault().post(new AlbumListLoadEvent(albums, DataType.onlineData));
                EventBus.getDefault().post(new AlbumListRefreshEvent(albums));
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                Log.e(TAG, "fetch album list from webservice error", t.getCause());

                EventBus.getDefault().post(new AlbumListLoadEvent(new ArrayList<Album>(), DataType.invalidData));
            }
        });
    }
}
