package com.mehdijoe.demoapplication.model.api;

import com.mehdijoe.demoapplication.model.entity.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Joe on 24/10/2018.
 */

public interface PhotoService {

    @GET("photos")
    Call<List<Photo>> getAllPhotos(@Query("albumId") int albumId);

}
