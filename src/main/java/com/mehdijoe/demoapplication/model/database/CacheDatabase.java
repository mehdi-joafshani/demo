package com.mehdijoe.demoapplication.model.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.mehdijoe.demoapplication.model.database.dao.AlbumDao;
import com.mehdijoe.demoapplication.model.database.dao.PhotoDao;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.entity.Photo;

/**
 * Created by Joe on 24/10/2018.
 */
@Database(entities = {Album.class, Photo.class}, version = 1)
public abstract class CacheDatabase extends RoomDatabase {

    public abstract AlbumDao albumDao();
    public abstract PhotoDao photoDao();

}
