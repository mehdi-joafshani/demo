package com.mehdijoe.demoapplication.model.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mehdijoe.demoapplication.model.entity.Photo;

import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

@Dao
public interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Photo> photos);

    @Query("DELETE FROM photo WHERE albumId = :albumId")
    void deleteAllAlbumPhotos(int albumId);

    @Query("SELECT * FROM photo WHERE albumId = :albumId")
    List<Photo> getPhotos(int albumId);


    /**
     * Getting cover photos
     *
     * @return a list of photos which does not have repeated albumId
     */
    @Query("SELECT a.* FROM photo a" +
            " INNER JOIN (SELECT albumId, MIN(id) as id FROM photo" +
            "  GROUP BY albumId" +
            ") AS b" +
            "  ON a.albumId = b.albumId AND a.id = b.id;")
    List<Photo> getPhotoPerAlbum();
}
