package com.mehdijoe.demoapplication.model.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mehdijoe.demoapplication.model.entity.Album;

import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

@Dao
public interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Album> albums);

    @Query("DELETE FROM album")
    void deleteAll();

    @Query("SELECT * FROM album")
    List<Album> getAllAlbums();
}
