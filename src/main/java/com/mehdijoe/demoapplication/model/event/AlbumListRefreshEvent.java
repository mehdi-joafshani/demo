package com.mehdijoe.demoapplication.model.event;

import com.mehdijoe.demoapplication.model.entity.Album;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public class AlbumListRefreshEvent {
    private List<Album> newAlbums;

    public AlbumListRefreshEvent(List<Album> newAlbums) {
        this.newAlbums = newAlbums;
    }

    public List<Album> getNewAlbums() {
        return newAlbums;
    }
}
