package com.mehdijoe.demoapplication.model.event;

import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.entity.Photo;

import java.util.List;

/**
 * Created by Joe on 24/10/2018.
 */

public class PhotoListRefreshEvent {
    private List<Photo> newPhotos;
    private int albumId;

    public PhotoListRefreshEvent(List<Photo> newPhotos, int albumId) {
        this.newPhotos = newPhotos;
        this.albumId = albumId;
    }

    public int getAlbumId() {
        return albumId;
    }

    public List<Photo> getNewPhotos() {
        return newPhotos;
    }
}
