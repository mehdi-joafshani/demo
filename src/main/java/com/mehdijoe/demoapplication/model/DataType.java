package com.mehdijoe.demoapplication.model;

/**
 * Created by Joe on 24/10/2018.
 */

/**
 *  This enum illustrates the way which data has been fetched/created
 */
public enum DataType{
    offlineData,
    onlineData,
    invalidData,
    notSpecified
}
