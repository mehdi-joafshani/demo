package com.mehdijoe.demoapplication.model.repository;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.api.PhotoRemoteRepository;
import com.mehdijoe.demoapplication.model.database.CacheDatabase;
import com.mehdijoe.demoapplication.model.database.dao.PhotoDao;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.model.event.PhotoListRefreshEvent;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumsCoverPhotoLoadEvent;
import com.mehdijoe.demoapplication.viewmodel.event.PhotoListLoadEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public interface PhotoRepositoryHandler {
    void onAlbumListRefereshed(@NonNull PhotoListRefreshEvent event);
    void getPhotos(int albumId);
    void refreshPhotos(int albumId);
    void getCoverPhotoPerAlbum();
}
