package com.mehdijoe.demoapplication.model.repository;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.api.PhotoRemoteRepository;
import com.mehdijoe.demoapplication.model.api.PhotoRemoteRepositoryHandler;
import com.mehdijoe.demoapplication.model.database.CacheDatabase;
import com.mehdijoe.demoapplication.model.database.dao.PhotoDao;
import com.mehdijoe.demoapplication.model.entity.Photo;
import com.mehdijoe.demoapplication.model.event.PhotoListRefreshEvent;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumsCoverPhotoLoadEvent;
import com.mehdijoe.demoapplication.viewmodel.event.PhotoListLoadEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public class PhotoRepository implements PhotoRepositoryHandler{
    private static final String TAG = "PhotoRepository";

    private PhotoDao photoDao;
    private PhotoRemoteRepositoryHandler photoRemoteRepositoryHandler;

    @Inject
    public PhotoRepository(CacheDatabase cacheDatabase, PhotoRemoteRepositoryHandler photoRemoteRepositoryHandler) {
        photoDao = cacheDatabase.photoDao();
        this.photoRemoteRepositoryHandler = photoRemoteRepositoryHandler;

        EventBus.getDefault().register(this);
    }

    @Override
    protected void finalize() throws Throwable {
        EventBus.getDefault().unregister(this);
        super.finalize();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlbumListRefereshed(@NonNull PhotoListRefreshEvent event) {
        Log.d(TAG, "onAlbumListRefereshed");
        replacePhotos(event.getNewPhotos(), event.getAlbumId());
    }

    private void replacePhotos(List<Photo> newPhotos, int albumId) {
        new PhotoRepository.ReplacePhotosTask(photoDao, albumId).execute(newPhotos);
    }

    private void insertPhotos(List<Photo> photos) {
        new PhotoRepository.InsertTask(photoDao).execute(photos);
    }

    private void deleteAllPhotosInAlbum(int albumId) {
        new PhotoRepository.DeleteAllTask(photoDao, albumId).execute();
    }

    public void getPhotos(int albumId) {
        new PhotoRepository.GetAllTask(photoDao, photoRemoteRepositoryHandler, albumId).execute();
    }

    public void refreshPhotos(int albumId) {
        photoRemoteRepositoryHandler.getPhotos(albumId);
    }

    public void getCoverPhotoPerAlbum(){
        new PhotoRepository.GetPhotoPerAlbumTask(photoDao).execute();

    }

    private static class InsertTask extends AsyncTask<List<Photo>, Void, Void> {
        private PhotoDao photoDao;

        public InsertTask(PhotoDao photoDao) {
            this.photoDao = photoDao;
        }

        @Override
        protected Void doInBackground(List<Photo>[] lists) {
            photoDao.insert(lists[0]);
            return null;
        }
    }

    private static class ReplacePhotosTask extends AsyncTask<List<Photo>, Void, Void> {
        private PhotoDao photoDao;
        private int albumId;

        public ReplacePhotosTask(PhotoDao photoDao, int albumId) {
            this.photoDao = photoDao;
            this.albumId = albumId;
        }

        @Override
        protected Void doInBackground(List<Photo>[] lists) {
            photoDao.deleteAllAlbumPhotos(albumId);
            photoDao.insert(lists[0]);
            return null;
        }
    }

    private static class DeleteAllTask extends AsyncTask<Void, Void, Void> {
        private PhotoDao photoDao;
        private int albumId;

        public DeleteAllTask(PhotoDao photoDao, int albumId) {
            this.photoDao = photoDao;
            this.albumId = albumId;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            photoDao.deleteAllAlbumPhotos(albumId);
            return null;
        }
    }

    private static class GetAllTask extends AsyncTask<Void, Void, List<Photo>> {
        private PhotoDao photoDao;
        private PhotoRemoteRepositoryHandler photoRemoteRepositoryHandler;
        private int albumId;

        public GetAllTask(PhotoDao photoDao, PhotoRemoteRepositoryHandler photoRemoteRepositoryHandler, int albumId) {
            this.photoDao = photoDao;
            this.photoRemoteRepositoryHandler = photoRemoteRepositoryHandler;
            this.albumId = albumId;
        }

        @Override
        protected List<Photo> doInBackground(Void... voids) {
            List<Photo> photos = photoDao.getPhotos(albumId);

            return photos;
        }

        @Override
        protected void onPostExecute(List<Photo> photos) {
            if (photos.size() > 0) {
                EventBus.getDefault().post(new PhotoListLoadEvent(photos, DataType.offlineData));
            }else{
                photoRemoteRepositoryHandler.getPhotos(albumId);
            }

            super.onPostExecute(photos);
        }
    }

    private static class GetPhotoPerAlbumTask extends AsyncTask<Void, Void, List<Photo>> {
        private PhotoDao photoDao;

        public GetPhotoPerAlbumTask(PhotoDao photoDao) {
            this.photoDao = photoDao;
        }

        @Override
        protected List<Photo> doInBackground(Void... voids) {
            List<Photo> photos = photoDao.getPhotoPerAlbum();

            return photos;
        }

        @Override
        protected void onPostExecute(List<Photo> photos) {
            if (photos.size() > 0) {
                EventBus.getDefault().post(new AlbumsCoverPhotoLoadEvent(photos, DataType.offlineData));
            }

            super.onPostExecute(photos);
        }
    }
}
