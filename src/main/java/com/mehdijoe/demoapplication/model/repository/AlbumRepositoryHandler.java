package com.mehdijoe.demoapplication.model.repository;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.api.AlbumRemoteRepository;
import com.mehdijoe.demoapplication.model.database.CacheDatabase;
import com.mehdijoe.demoapplication.model.database.dao.AlbumDao;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.event.AlbumListRefreshEvent;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumListLoadEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public interface AlbumRepositoryHandler {

    void onAlbumListRefereshed(@NonNull AlbumListRefreshEvent event);
    void getAlbums();
    void refreshAlbums();
}