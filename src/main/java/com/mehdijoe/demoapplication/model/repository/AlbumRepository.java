package com.mehdijoe.demoapplication.model.repository;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mehdijoe.demoapplication.model.DataType;
import com.mehdijoe.demoapplication.model.api.AlbumRemoteRepository;
import com.mehdijoe.demoapplication.model.api.AlbumRemoteRepositoryHandler;
import com.mehdijoe.demoapplication.model.database.CacheDatabase;
import com.mehdijoe.demoapplication.model.database.dao.AlbumDao;
import com.mehdijoe.demoapplication.model.entity.Album;
import com.mehdijoe.demoapplication.model.event.AlbumListRefreshEvent;
import com.mehdijoe.demoapplication.viewmodel.event.AlbumListLoadEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Joe on 24/10/2018.
 */

public class AlbumRepository implements AlbumRepositoryHandler {
    private static final String TAG = "AlbumRepository";

    private AlbumDao albumDao;
    private AlbumRemoteRepositoryHandler albumRemoteRepositoryHandler;

    @Inject
    public AlbumRepository(CacheDatabase cacheDatabase, AlbumRemoteRepositoryHandler albumRemoteRepositoryHandler) {
        albumDao = cacheDatabase.albumDao();
        this.albumRemoteRepositoryHandler = albumRemoteRepositoryHandler;

        EventBus.getDefault().register(this);
    }

    @Override
    protected void finalize() throws Throwable {
        EventBus.getDefault().unregister(this);
        super.finalize();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlbumListRefereshed(@NonNull AlbumListRefreshEvent event) {
        Log.d(TAG, "onAlbumListRefereshed");
        replaceAlbums(event.getNewAlbums());
    }

    private void insertAlbums(List<Album> albums) {
        new InsertTask(albumDao).execute(albums);
    }

    private void replaceAlbums(List<Album> newAlbums) {
        new ReplaceAlbumsTask(albumDao).execute(newAlbums);
    }

    private void deleteAllAlbums() {
        new DeleteAllTask(albumDao).execute();
    }

    public void getAlbums() {
        new GetAllTask(albumDao, albumRemoteRepositoryHandler).execute();
    }

    public void refreshAlbums() {
        albumRemoteRepositoryHandler.getAlbums();
    }

    private static class InsertTask extends AsyncTask<List<Album>, Void, Void> {
        private AlbumDao albumDao;

        public InsertTask(AlbumDao albumDao) {
            this.albumDao = albumDao;
        }

        @Override
        protected Void doInBackground(List<Album>[] lists) {
            albumDao.insert(lists[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private static class ReplaceAlbumsTask extends AsyncTask<List<Album>, Void, List<Album>> {
        private AlbumDao albumDao;

        public ReplaceAlbumsTask(AlbumDao albumDao) {
            this.albumDao = albumDao;
        }

        @Override
        protected List<Album> doInBackground(List<Album>[] lists) {
            albumDao.deleteAll();
            return lists[0];
        }

        @Override
        protected void onPostExecute(List<Album> lists) {
            new InsertTask(albumDao).execute(lists);
            super.onPostExecute(lists);
        }
    }

    private static class DeleteAllTask extends AsyncTask<Void, Void, Void> {
        private AlbumDao albumDao;

        public DeleteAllTask(AlbumDao albumDao) {
            this.albumDao = albumDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            albumDao.deleteAll();
            return null;
        }
    }

    private static class GetAllTask extends AsyncTask<Void, Void, List<Album>> {
        private AlbumDao albumDao;
        private AlbumRemoteRepositoryHandler albumRemoteRepositoryHandler;

        public GetAllTask(AlbumDao albumDao, AlbumRemoteRepositoryHandler albumRemoteRepositoryHandler) {
            this.albumDao = albumDao;
            this.albumRemoteRepositoryHandler = albumRemoteRepositoryHandler;
        }

        @Override
        protected List<Album> doInBackground(Void... voids) {
            List<Album> listMutableLiveData = albumDao.getAllAlbums();

            return listMutableLiveData;
        }

        @Override
        protected void onPostExecute(List<Album> albums) {
            if (albums != null && albums.size() > 0) {
                EventBus.getDefault().post(new AlbumListLoadEvent(albums, DataType.offlineData));
            } else {
                albumRemoteRepositoryHandler.getAlbums();
            }

            super.onPostExecute(albums);
        }
    }
}