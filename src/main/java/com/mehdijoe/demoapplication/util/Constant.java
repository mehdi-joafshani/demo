package com.mehdijoe.demoapplication.util;

/**
 * Created by Joe on 24/10/2018.
 */

public class Constant {
    public static final String BASE_URL = "https://jsonplaceholder.typicode.com";
    public static final String NOT_INITIALIZED_IMAGE_PATH = "nothing";
}
