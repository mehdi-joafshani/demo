package com.mehdijoe.demoapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.ComponentNameMatchers;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.mehdijoe.demoapplication.view.activity.MainActivity;
import com.mehdijoe.demoapplication.view.adapter.AlbumAdapter;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;

/**
 * Created by Joe on 24/10/2018.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest {
    @Rule
    public IntentsTestRule<MainActivity> activityTestRule = new IntentsTestRule<>(MainActivity.class);

    @Test
    public void openAlbum() {
        int numberOfAlbums = ((RecyclerView) activityTestRule.getActivity().findViewById(R.id.recycler))
                .getAdapter().getItemCount();

        for (int i = 0; i < numberOfAlbums; i++) {
            onView(ViewMatchers.withId(R.id.recycler))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(i, click()));

            Intents.intending(IntentMatchers.hasComponent(ComponentNameMatchers.hasClassName("com.mehdijoe.demoapplication.view.activity.AlbumActivity")));

            pressBack();
        }
    }
}
