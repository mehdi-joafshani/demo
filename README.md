A Demo Android Application to fetch the list of albums and photos. 
The app uses the API in http://jsonplaceholder.typicode.com . 
The app caches API results in local database using ROOM ORM.

Possible Improvements:
- Paging lists (both api and local) for better performance in cases network connection is poor or larg lists
- Using RxJava/Retrofit/ROOM in repository instead of current asyncTask
- Using Broadcas receiver to fetch data - if it's necessary - after internet connection establishment
- Adding other pages/entities such as user, post in pages which can navigate to each other
- Better interactive UI
- Identify user somehow whether the shown data is offline or not